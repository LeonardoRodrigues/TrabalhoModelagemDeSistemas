package modelo;

public class Produto {
	
	private int codigo;
	private String descricao;
	private float valor;
	private int quantidade;
	private String dataCadastro;
	
	public Produto() {}
	
	public Produto(int codigo,String descricao, float valor,int quantidade,String dataCadastro) {
		this.codigo=codigo;
		this.descricao = descricao;
		this.valor = valor;
		this.quantidade = quantidade;
		this.dataCadastro= dataCadastro;
	}
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	public String getDataCadastro() {
		return dataCadastro;
	}
	public void setDataDeCadastro(String dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public void baixarProduto(int quantBaixada){
		if (quantBaixada <= this.quantidade){
			this.quantidade = this.quantidade - quantBaixada;
		}	
	}
	

}
