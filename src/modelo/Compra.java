package modelo;

import java.util.ArrayList;
import java.util.List;

public class Compra {
	
	private int numero;
	private String dataCompra;
	
	private List<ItensCompra> itens = new ArrayList<ItensCompra>();
	
	public Compra(int numero) {
		this.numero = numero;
	}
	
	public void addItensCompra(ItensCompra item){
		itens.add(item);
	}
	
	public void removeItensCompraPorDescricao(String descricao){
		for (ItensCompra item: itens){
			if (item.getDescricao().equals(descricao)){
				itens.remove(item);
				break;
			}
		}
	}	

	
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getDataCompra() {
		return dataCompra;
	}
	public void setDataCompra(String dataCompra) {
		this.dataCompra = dataCompra;
	}
	
	public float totalizarItens(){
		float total=0;
		for (ItensCompra item:itens){
			total = total + item.getQuantidade()*item.getValor();
		}
		return total;
	}
	
	public List<ItensCompra> getItens(){
		return itens;
	}

}
