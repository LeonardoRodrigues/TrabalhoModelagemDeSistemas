package modelo;

public class ItensCompra {
	
	private int codigo;
	private int quantidade;
	
	private Produto produto= new Produto();
	
	public  ItensCompra(String descricao,int codigo, int quantidade, float valor) {
		produto.setDescricao(descricao);
		produto.setCodigo(codigo);
		produto.setValor(valor);
		this.quantidade = quantidade;
	}
	
	public String getDescricao() {
		return produto.getDescricao();
	}
	
	public void setDescricao(String descricao) {
		produto.setDescricao(descricao);
	}

	
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	
	public float getValor() {
		return produto.getValor();
	}
	
	public void setValor(float valor) {
		produto.setValor(valor);
	}
	

}
