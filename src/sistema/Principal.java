package sistema;

import controlador.Sistema;
import modelo.Endereco;


public class Principal {
	public static void main(String[] args) {
	
	Sistema controlador = Sistema.getInstance();
	controlador.cadastrarProduto(1,"Iphone 7", 2500.0F,500,"15/10/2017");
	controlador.cadastrarProduto(2,"Samsung j7",800.0F,150, "20/11/2016" );
	controlador.cadastrarProduto(3,"Moto g",750.0F,250, "08/03/2018" );
	controlador.cadastrarProduto(4,"Iphone 7s",3000.0F,300,"20/12/2017" );
	
	controlador.cadastrarCliente(888-222-1, "Leonardo","96482-8552",Endereco.comercial,"24/04/2004" );
	controlador.cadastrarCliente(985-762-8,"Jubileu", "98456-7321",Endereco.comercial,"18/06/2017" );
	
	System.out.println("\n\n");		
	for (String str: controlador.listarProdutosCadastrados()){
		System.out.println(str);
	}
	
	for (String str: controlador.listarClientesCadastrados()){
		System.out.println(str);
	}

	
	controlador.selecionarClientePorNome("Leonardo");
	controlador.abrirCompra(1);
	controlador.venderProduto(1, "Iphone 7s", 2);
	controlador.venderProduto(1, "Moto g", 5);
    System.out.println("Total da compra 1: "+controlador.totalizarVenda(1));
    controlador.selecionarClientePorNome("Jubileu");
	controlador.abrirCompra(2);
	controlador.venderProduto(2, "Iphone 7", 5);
	controlador.venderProduto(2, "Samsung j7 ", 10);
    System.out.println("Total da compra 2: "+controlador.totalizarVenda(2));
	
}


}