package controlador;

import java.util.ArrayList;
import java.util.List;

import modelo.Cliente;
import modelo.Compra;
import modelo.Endereco;
import modelo.ItensCompra;
import modelo.Produto;



public class Sistema {
	
	private static Sistema sdv=null;
	private Cliente clienteAtual=null;
	private List<Produto> cadastroProduto = new ArrayList<Produto>();
	private List<Cliente> cadastroCliente = new ArrayList<Cliente>();
	
	private Sistema(){}
	
	public static Sistema getInstance(){
		if (sdv == null){
			sdv = new Sistema();
			return sdv;
		} else
			throw new RuntimeException("Controlador ja foi criado!!");
	}
	
	public void cadastrarProduto(int codigo,String descricao,float valor,int quantidadeEmEstoque,String dataCadatro){
		Produto prod = new Produto(codigo,descricao,valor,quantidadeEmEstoque,dataCadatro);
		boolean achou = false;
		for (Produto produto: cadastroProduto){
			if (produto.getDescricao().equals(descricao)){
				achou=true;
				break;
			}
		}
		if (! achou){
			cadastroProduto.add(prod);
		}
	}
	
	public List<String> listarProdutosCadastrados(){
		List<String> produtos = new ArrayList<String>();
		for (Produto produto: cadastroProduto){
			produtos.add(produto.getDescricao());
		}
		return produtos;
	}
	
	public void cadastrarCliente( int cpf,String nome , String telefone, Endereco tipo, String dataCadatro){
		Cliente clie = new Cliente( cpf , nome , telefone, dataCadatro, tipo);
		boolean achou = false;
		for (Cliente cliente: cadastroCliente){
			if (cliente.getNome().equals(nome)){
				achou=true;
				break;
			}
		}
		if (! achou ){
			cadastroCliente.add(clie);
		}
	}
	
	public List<String> listarClientesCadastrados(){
		List<String> clientes = new ArrayList<String>();
		for (Cliente cliente: cadastroCliente){
			clientes.add(cliente.getNome());
		}
		return clientes;
	}
	
	public void selecionarClientePorNome(String nome){
		for (Cliente clie: cadastroCliente){
			if (clie.getNome().equals(nome)){
				clienteAtual=clie;
			}
		}
	}
	
	public void abrirCompra(int numero){
		Compra compra = new Compra(numero);
		if (clienteAtual != null){
			clienteAtual.adicionarCompra(compra);
		} else
		   throw new RuntimeException("Cliente não foi selecionado!!");
	}
	public void venderProduto(int codigoCompra,String descricaoProduto,int quantidade){
		if (clienteAtual != null){
			Compra compraAtual = clienteAtual.pegarCompraPorNumero(codigoCompra);
			if (compraAtual != null){
				for (Produto produto: cadastroProduto) {
					if (produto.getDescricao().equals(descricaoProduto)) {
						compraAtual.addItensCompra(new ItensCompra(descricaoProduto,quantidade,quantidade, produto.getValor()));			
						produto.baixarProduto(quantidade);
					} 
				}
			} else
			 throw new RuntimeException("Compra nao foi aberta!!");				
		} else
			throw new RuntimeException("Cliente nao foi selecionado!!");
	}
	public float totalizarVenda(int codigoCompra) {
		float total=0;
		if (clienteAtual != null) {
			Compra compraAtual = clienteAtual.pegarCompraPorNumero(codigoCompra);
			if (compraAtual != null){
				total = compraAtual.totalizarItens();
			} else
			 throw new RuntimeException("Compra n�o foi aberta!!");				
		} else
			throw new RuntimeException("Cliente n�o foi selecionado!!");
		return total;
	}


}
